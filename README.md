# OpenML dataset: Popular-Halloween-2020--Costumes-Amazon-Reviews

https://www.openml.org/d/43360

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
So it's Halloween again dear Kagglers!
And what better way of celebrating than with some NLP!
The dataset brings you the reviews of popular Halloween costumes sold on amazon as of November 2020.
Content
The dataset contains popular costumes from the Amazon website, for each costume there are user review texts including the review title and the review score, also you will find the publishing date and location.
The data hasn't been preprocessed in any way so I think it can be a great exercise for aspiring data scientists who are looking to sharpen their skills in text preprocessing skills and feature extraction skills.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43360) of an [OpenML dataset](https://www.openml.org/d/43360). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43360/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43360/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43360/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

